﻿using System;
using System.Threading;
using GameOfLife.Core;

namespace GameOfLife2018.CLR {
    class GameOfLife2018 {
        static Board _board;
        static Random _rnd;

        static void Main(string[] args) {
            Console.Title = "Game of life 2018";

            _board = new Board(10, 10);
            _rnd = new Random();

            RandomizeBoard();

            while (true) {
                PrintBoard();                
                Thread.Sleep(1000);
                _board.UpdateBoard();
            }
        }

        static void PrintBoard() {
            Console.Clear();

            for (int y = 0; y < _board.Height; y++) {
                for (int x = 0; x < _board.Width; x++) {
                    Console.Write(_board.Field[y, x].IsAlive ? "1 " : "0 ");
                }

                Console.WriteLine();
            }
        }

        static void RandomizeBoard() {
            foreach (Cell cell in _board.Field) {
                cell.IsAlive = _rnd.Next(2) == 1 ? true : false;
            }
        }
    }
}
