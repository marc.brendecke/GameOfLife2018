﻿using System;

namespace GameOfLife.Core {
    public class Board {
        public Cell[,] Field { get; private set; }
        public int Width => Field.GetLength(1);
        public int Height => Field.GetLength(0);

        public Board(int width = 5, int height = 5) {
            Field = new Cell[height > 0 ? height : 1, width > 0 ? width : 1];
            Initialize();
        }

        private void Initialize() {
            for (int y = 0; y < Height; y++) {
                for (int x = 0; x < Width; x++) {
                    Field[y, x] = new Cell(this, x, y) {
                        IsAlive = false
                    };
                }
            }
        }

        public virtual void UpdateBoard() {
            OnBeforeUpdate(new EventArgs());
            OnUpdate(new EventArgs());
        }

        public event EventHandler<EventArgs> BeforeUpdate;
        protected virtual void OnBeforeUpdate(EventArgs e) => BeforeUpdate?.Invoke(this, e);

        public event EventHandler<EventArgs> Update;
        protected virtual void OnUpdate(EventArgs e) => Update?.Invoke(this, e);
    }
}
