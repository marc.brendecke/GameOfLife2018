﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameOfLife.Core
{
    public class Cell
    {
        private Board _board;
        private bool _nextIsAlive;
        private IEnumerable<Cell> _neighbors;

        public int X { get; private set; }
        public int Y { get; private set; }
        public bool IsAlive { get; set; }

        public Cell(Board board, int x, int y) {
            _board = board;
            _board.BeforeUpdate += BoardBeforeUpdate;
            _board.Update += BoardUpdate;

            X = x;
            Y = y;
            _nextIsAlive = false;
        }

        private IEnumerable<Cell> GetNeighbors() {
            if (_neighbors is null) {                
                _neighbors = new List<Cell>();

                for (int y = -1; y <= 1; y++) {                    
                    for (int x = -1; x <= 1; x++) {
                        if (x == 0 && y == 0) continue;

                        int xx = X + x;
                        if (xx < 0) xx += _board.Width;
                        else if (xx >= _board.Width) xx -= _board.Width;

                        int yy = Y + y;
                        if (yy < 0) yy += _board.Height;
                        else if (yy >= _board.Height) yy -= _board.Height;

                        ((List<Cell>)_neighbors).Add(_board.Field[yy, xx]);
                    }
                }                
            }

            return _neighbors;
        }

        private void BoardUpdate(object sender, EventArgs e) {
            IsAlive = _nextIsAlive;
        }

        private void BoardBeforeUpdate(object sender, EventArgs e) {
            int neighborsWhoLife = 0;

            foreach (Cell neighbor in GetNeighbors()) {
                neighborsWhoLife += neighbor.IsAlive ? 1 : 0;
            }

            if (IsAlive && (neighborsWhoLife < 2 || neighborsWhoLife > 3)) _nextIsAlive = false;
            else if (!IsAlive && neighborsWhoLife == 3) _nextIsAlive = true;
        }
    }
}
